## Name
nous2influx

## Description
collects values from NOUS A1 T sockets an sends them to influxdb. You may use your grafana to set up your individual dashboard.

## Installation
this is mentioned to run on RaspberryPi. 
Each Version is suitable, needs no powerful hardware.

- **Install influxdb** with `sudo apt install influxdb`. 
- `sudo apt install influxdb-client` for terminal support
- configure influxdb with user = "nous",password = "passwort" and dbname = "klimawandel"

```
CREATE USER "nous" WITH PASSWORD 'passwort' WITH ALL PRIVILEGES
CREATE DATABASE "klimawandel"
GRANT ALL ON "klimawandel" TO "nous"
```

- **Install Grafana** 
```
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
sudo apt-get update
sudo apt-get install -y grafana
sudo /bin/systemctl enable grafana-server
sudo /bin/systemctl start grafana-server
```
- and setup data source influxdb. 
- enhance your python3.x installation with 

-- `sudo apt-get install python3-influxdb`

- Call nous2influx.py in `sudo nano /etc/crontab` every 2 minutes.

## code
`url_4041 = 'http://192.168.20.207/cm?cmnd=Status%208' # SunMan 375Wp`
I use the last 4 digits from mac-adress in the url_xxxx and create a string including ip-adress and commands for the specific socket. You can have couple of sockets in one python skript.


## Support
pf@nc-x.com

## Roadmap


## Contributing
open to contributions
Ingmar did a more detailled description in german: 
[Tasmota Steckdosen Energie Monitoring](https://www.byteyourlife.com/haushaltsgeraete/tasmota-wlan-steckdose-mit-influx-db-und-grafana/9069)


## Authors and acknowledgment

## License
MIT

## Project status
under development
